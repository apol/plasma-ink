// SPDX-FileCopyrightText: 2022 Aleix Pol <apol@kde.org>
// SPDX-License-Identifier: GPL-2.0-or-later

loadTemplate("org.kde.plasma.desktop.defaultPanel")

var desktopsArray = desktopsForActivity(currentActivity());
for( var j = 0; j < desktopsArray.length; j++) {
    desktopsArray[j].wallpaperPlugin = 'org.kde.color';
}

