# SPDX-FileCopyrightText: 2019 Aleix Pol <apol@kde.org>
# SPDX-License-Identifier: GPL-2.0-or-later

[Desktop Entry]
Exec=@CMAKE_INSTALL_FULL_LIBEXECDIR@/plasma-dbus-run-session-if-needed  ${CMAKE_INSTALL_FULL_BINDIR}/startplasma-ink
TryExec=${CMAKE_INSTALL_FULL_BINDIR}/startplasma-ink
DesktopNames=KDE
Name=Plasma Ínk
Comment=Plasma Ink by KDE
X-KDE-PluginInfo-Version=${PROJECT_VERSION}
